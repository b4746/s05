-- Inserting Records
-- Command -> INSEERT INTO tbl_name(column) VALUES ("col_value");
-- MySQL Year format: YYYY-MM-DD

-- Insert values in artists table
INSERT INTO artists(name) VALUES ("Rivermaya");
INSERT INTO artists(name) VALUES ("Psy");

-- Insert values in albums table
INSERT INTO albums(album_title, date_released, artist_id) VALUES ("Psy 6", "2012-1-1", 2);
INSERT INTO albums(album_title, date_released, artist_id) VALUES ("Trip", "1996-1-1", 1);

-- Insert values in songs table
INSERT INTO songs(song_name, length, genre, album_id) VALUES ("Gangnam Style", 253, "K-POP", 4);
INSERT INTO songs(song_name, length, genre, album_id) VALUES ("Kundiman", 234, "OPM", 2)
INSERT INTO songs(song_name, length, genre, album_id) VALUES ("Kisapmata", 279, "OPM", 2)

-- Selecting Records
-- Display the title and genre of all the songs.
-- (*) = all
SELECT song_name, genre FROM songs;

-- Display all columns in the songs table.
-- (*) = all
SELECT * FROM songs;

-- Display the title of all the OPM songs.
SELECT song_name FROM songs WHERE genre = "OPM";

-- We can use AND and OR keyword for multiple exppressions in the WHERE clause.

-- Display the title and length of the OPM songs that are more than 2 minutes.
SELECT song_name, length FROM songs WHERE length > 200 AND genre = "OPM";
 

-- Update the length of Kundiman to 240 seconds

UPDATE songs SET length = 240 WHERE song_name = "Kundiman";

UPDATE songs SET length = 250 WHERE song_name = "Kisapmata";

UPDATE songs SET length = 200 WHERE song_name = "Gangnam Style";


-- Deleting Records
-- Deleete all OPM songs that are equal or more than 2 minutes
DELETE FROM songs WHERE genre = "OPM" AND length >= 200;



-- To change the name of the table.
ALTER TABLE [table name] RENAME TO [new table name]


-- Add new records

-- Add 5 artists, 2 albums each, 2 songs per album.

INSERT INTO artists (name) VALUES ("Taylor Swift");
INSERT INTO artists (name) VALUES ("Lady Gaga");
INSERT INTO artists (name) VALUES ("Justin Bieber");
INSERT INTO artists (name) VALUES ("Ariana Grande");
INSERT INTO artists (name) VALUES ("Bruno Mars");

-- Taylor Swift
INSERT INTO albums(album_title, date_released, artist_id) VALUES ("Fearless", "2008", 3);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Fearless", 246, "Pop", 3);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Love story", 213, "Country Pop", 3);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Red", "2012-03-20", 3);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("State of Grace", 313, "Rock, Alternative Rock, Arena Rock", 4);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Red", 204, "Country", 4);

-- Lady Gaga
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("A Star Is Born", "2018-06-24", 4);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Black Eyes", 221, "Rock and roll", 5);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Shallo", 201, "Country, Rock, Folk Rock", 5);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Born This Way", "2011-03-18", 4);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Born This Way", 252, "Electropop", 6);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Hair", 320, "Electropop, rock", 6);

-- Justin Bieber
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Purpose", "2015-04-10", 5);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Sorry", 232, "Dancehall-poptropical", 7);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Believe", "2012-12-20", 5);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Boyfriend", 251, "Pop", 8);

-- Ariana Grande
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Dangerous Woman", "2016-08-14", 6);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Into You", 242, "Pop", 9);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Thank U, Next", "2019-11-20", 6);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Thank U, Next", 236, "Pop, R&B", 10);

-- Bruno Mars
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("24K Magic", "2016-04-24", 7);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("24K Magic", 207, "Funk, Disco, R&B", 11);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Earth to Mars", "2011-10-17", 7);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Lost", 232, "Pop", 12);

-- Advanced Selects

-- Exclude records.

SELECT * FROM songs WHERE id != 11;

-- Greatter or Less than
SELECT * FROM songs WHERE id < 11;

-- Greater or equal to
SELECT * FROM songs WHERE id >= 11;

-- Get specific IDs (OR).
SELECT * FROM songs WHERE id = 1 OR id = 3 OR id = 5;

-- Get specific IDs (IN).
SELECT * FROM songs WHERE id IN (1, 3, 5);
SELECT * FROM songs WHERE genre IN ("Pop", "K-POP");

-- Combining conditions.
SELECT * FROM songs WHERE album_id = 4 AND id < 8;

-- Find partial matches (ends with the letter "a")
SELECT * FROM songs WHERE song_name LIKE "%a";  

-- Find partial matches (starts with the letter "a")
SELECT * FROM songs WHERE song_name LIKE "a%";

-- Find partial matches (in between with the letter "a")
SELECT * FROM songs WHERE song_name LIKE "%a%";

SELECT * FROM albums WHERE date_released LIKE "%201%";

-- Sorting records
SELECT * FROM songs ORDER BY song_name ASC;
SELECT * FROM songs ORDER BY song_name DESC;

-- Gettiing distinct records. 
SELECT DISTINCT genre FROM songs;


-- Table Joins
-- Combine artists and albums table.

SELECT * FROM artists
	JOIN albums ON artists.id = albums.artist_id;

-- C.ombine more than two tables.
SELECT * FROM artists
	JOIN albums ON artist.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id;

-- Select columns to be included per table.

SELECT artists.name, albums.album_title
	FROM artists JOIN albums ON artists.id = albums.artist_id;

-- Show artists without records on the right side of the joined table.

SELECT * FROM artists
	LEFT JOIN albums ON artists.id = albums.artist_id;