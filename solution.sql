-- ACTIVITY

-- 1. Return the customerName of the customers who are from the Philippines.
		Answer:
			SELECT customerName FROM customers WHERE country = Philippines;

-- 2. Return the contactLastName and contactFirstName of customers with name "La Rochelle Gifts".
		Answer:
			SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

-- 3. Return the product name and MSRP of the product named "The Titanic".
		Answer:
			SELECT productName, MSRP FROM products WHERE productName = "The Titanic";

-- 4. Return the first and last name of the employyee whose email is "jfirrelli@classicmodelcars".
		Answer:
			SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

-- 5. Return names of customers who have no registered state
		Answer:
			SELECT customerName FROM customers WHERE state IS NULL;

-- 6. Return the first name, last name, email of the employee whose last name is Patterson and first name is Steve.
		Answer:
			SELECT firstName, lastName, email FROM employees WHERE lastName = "Patterson" AND firstName = "Steve";

-- 7. Return custome name, country, and credit limit of customers whose countries are NOT USA and whose credit limit are greater than 3000.
		Answer:
			SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;

-- 8. Return the customer names of cusromers whose customer names don't have 'a' in them.
		Answer:
			SELECT customerName FROM customers WHERE customerName != "%a%";

-- 9. Return the customer numbers of orders whose comments contain the string 'DHL'.
		Answer:
			SELECT customerNumber FROM orders WHERE comments = "DHL";

-- 10. Return the product lines whose text description mentions thhe phrase 'state of the art'.
		Answer:
			SELECT productLine FROM productLines WHERE textDescription LIKE "%state of the art%";

-- 11. Return the countries of customers without duplication.
		Answer:
			SELECT DISTINCT country FROM customers;

-- 12. Return the statuses of orders without duplication.
		Answer:
			SELECT DISTINCT status FROM orders;

-- 13. Return the customer names and countries of the customers whose country is USA, France, or Canada.
		Answer:
			SELECT customerName, country FROM customers WHERE country IN ("USA", "France", "Canada");

-- 14. Return the first name, last name, and office's city of employees whose offices are in tokyo.
		Answer:
			SELECT employees.firstName, employees.lastName
				FROM employees JOIN offices ON employees.officeCode = offices.officeCode 5;

-- 15. Return the customer names of customers who were served by the employee named "LEslie Thompson".
		Answer:
			SELECT customers.customerName 
				FROM customers JOIN employees WHERE firstName = "Leslie" AND lastName = "Thompson";

-- 16. Return the product names and customer name of products ordered by "Baane Mini Imports".
		Answer:
			SELECT products.productName, customers.customerName
				FROM products JOIN customers WHERE customerName = "Baane Mini Imports";

-- 17. Return the employees' first names, employees' last names, customer' names, and offices' countries of transactions whose customers and offices are in the same country
		Answer:
			SELECT employees.firstName, employees.lastName, customers.customerName, offices.country JOIN customers ON customers.country = employees.country;

-- 18. Return the last names and first names of employees being supervised by "Annthony Bow".
		Answer:
			SELECT lastName, firstName FROM employees WHERE reportsTo = 1142;

-- 19. Return the product name and MSRP of the product with the highest MSRP.
		Answer:
			SELECT productName, MAX(MSRP) AS MSRP FROM products;

-- 20. Return thenumber of customers in the UK.
		Answer:
			SELECT COUNT(*) FROM customers WHERE country = "UK";

-- 21. Return the number of products per product line.
		Answer:
			SELECT productLine, COUNT(products) FROM productlines 
			    JOIN products ON productlines.productLine = products.productLine;


-- 22. Return the number of customers served be every employee.
		Answer:
			SELECT COUNT(*), employees.firstName, employees.lastName FROM customers 
			    JOIN employees ON employees.employeeNumber = customers.salesRepEmployeeNumber;

-- 23. Return the product name and quantity in stock of products that belong to the prooduct line "planes" with stock quantities less than 1000.
		Answer:
			SELECT productName, quantityInStock FROM products WHERE productLine = 'planes' AND quantityInStock < 1000;